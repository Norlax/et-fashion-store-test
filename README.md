# Frontend ET FASHION

Frontend para la empresa de moda ET Fashion, especializada en la distribucion y venta de productos de moda.

## Comenzando 🚀

### Pre-requisitos 📋

Despliegue Automatico

* Node v12.16.2
* React V17.0.1 

### Instalación 🔧

_Clonamos el repositorio_

```
git clone https://gitlab.com/Norlax/et-fashion-store-test.git
```

_Accedemos en a la carpeta_
```
cd et-fashion-store-test
```

_Instalamos las dependencias_
```
npm install
```

_Finalmente iniciamos el servidor local_
```
npm run start
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [React](https://es.reactjs.org) - Libreria utilizada para crear el proyecto
* [Materialize](https://materializecss.com) - Libreria para el diseño
* [Google Auth](https://www.npmjs.com/package/react-google-login) - Paquete de autentificacion de google

## Versionado 📌

V. 1.0

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Franklin Márquez** - *Initial work* - [@norlax_](https://twitter.com/norlax_)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto.
