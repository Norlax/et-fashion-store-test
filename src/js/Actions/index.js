import { ADD_INFO, USER_LOADED, USER_OUT, USERDATA_LOADED, DATA_REQUESTED } from "../Constant/action-types";

/*
* Envia la data del formulario de contacto
*/
export function addRequest(payload) {
    return {type: ADD_INFO, payload}
}

/*
* Esta funcion recibe una URL
* para generar una petición GET
*/
export function getData(url) {
    return {type: DATA_REQUESTED, payload: {url}}
}

/*
* Enviar data de inicio de sesión
*/
export function logIn(payload) {
    return { type: USER_LOADED, payload}
}

/*
* Cerrar sesión
*/
export function LogOut() {
    return {type: USER_OUT}
}

/*
* Obtener los datos del usuario
* que ha iniciado sesión
*/
export function getUser() {
    return { type: USERDATA_LOADED}
}